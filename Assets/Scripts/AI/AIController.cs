﻿using PinBall.Input;
using PinBall.SceneObjects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PinBall.AI
{
    public class AIController
    {
        private Table _table;
        private InputController _inputControll;

        public AIController(InputController inputController)
        {
            _inputControll = inputController;
        }

        public void EnableAI(Table table)
        {
            _table = table;
            table.Ball.OnControllElementDetected += Flip;
            table.Ball.OnControllElementDispose += BackFlip;
            table.BallPusher.OnBallStay += Push;
        }

        public void DisableAI()
        {
            if (_table == null)
                return;
            _table.Ball.OnControllElementDetected -= Flip;
            _table.Ball.OnControllElementDispose -= BackFlip;
            _table.BallPusher.OnBallStay -= Push;

        }

        private void Flip(Flipper flipper)
        {
            _inputControll.PressButton(flipper.InputName);
        }
        private void BackFlip(Flipper flipper)
        {
            _inputControll.UpButton(flipper.InputName);
        }

        private void Push(BallPusher ballPusher)
        {
            _table.BallPusher.PowerValue = Random.Range(0f, 0.3f);
            _inputControll.PressButton(ballPusher.ButtonControll);
        }
    }
}