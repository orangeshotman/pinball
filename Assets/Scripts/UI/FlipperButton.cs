﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
namespace PinBall.UI
{
    public class FlipperButton : Button, IPointerDownHandler, IPointerUpHandler
    {
        public event Action OnPointerDownEvent = () => { };
        public event Action OnPointerUpEvent = () => { };

        public void OnPointerDown(PointerEventData eventData)
        {
            OnPointerDownEvent();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnPointerUpEvent();
        }
    }
}