﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace PinBall.UI
{
    public class UIBehaviour : MonoBehaviour
    {
        [SerializeField]
        private FlipperButton LeftButton;
        [SerializeField]
        private FlipperButton RightButton;
        [SerializeField]
        private Button StartGame;
        [SerializeField]
        private Button StartGameWithAI;
        [SerializeField]
        private Button PushButton;
        [SerializeField]
        private GameObject StartPanel;
        [SerializeField]
        private GameObject GamePanel;
        [SerializeField]
        private GameObject PusherPanel;
        [SerializeField]
        private Scrollbar _powerIndicator;
        [SerializeField]
        private Button ExitButton;

        public void SetScrollAction(Action<float> action)
        {
            var scrollAction = new Scrollbar.ScrollEvent();
            scrollAction.AddListener(new UnityEngine.Events.UnityAction<float>(action));
            _powerIndicator.onValueChanged = scrollAction;
        }

        public void SetButtonsControll(Action leftButtonDownAction, Action rightButtonDownAction, Action leftButtonUpAction, Action rightButtonUpAction)
        {
            LeftButton.OnPointerDownEvent += leftButtonDownAction;
            LeftButton.OnPointerUpEvent += leftButtonUpAction;
            RightButton.OnPointerDownEvent += rightButtonDownAction;
            RightButton.OnPointerUpEvent += rightButtonUpAction;
        }

        public void SetExitButtonAction(Action exitAction)
        {
            var exitButtonAction = new Button.ButtonClickedEvent();
            exitButtonAction.AddListener(new UnityEngine.Events.UnityAction(exitAction));
            exitButtonAction.AddListener(new UnityEngine.Events.UnityAction(() =>
            {
                GamePanel.SetActive(false);
                StartPanel.SetActive(true);
                PusherPanel.SetActive(false);
            }));
            ExitButton.onClick = exitButtonAction;
        }

        public void SetStartMenuButtons(Action noAIButtonAction, Action withAIButtonsAction)
        {
            var startGameActions = new Button.ButtonClickedEvent();
            startGameActions.AddListener(new UnityEngine.Events.UnityAction(noAIButtonAction));
            startGameActions.AddListener(new UnityEngine.Events.UnityAction(()=>{
                StartPanel.SetActive(false);
                GamePanel.SetActive(true);
            }));
            StartGame.onClick = startGameActions;
            var startWithAIActions = new Button.ButtonClickedEvent();
            startWithAIActions.AddListener(new UnityEngine.Events.UnityAction(withAIButtonsAction));
            startWithAIActions.AddListener(new UnityEngine.Events.UnityAction(() =>
            {
                StartPanel.SetActive(false);
                GamePanel.SetActive(true);
            }));
            StartGameWithAI.onClick = startWithAIActions;
        }

        public void SetActivePusherPanel(bool active)
        {
            PusherPanel.SetActive(active);
        }

        public void SetPushButtonAction(Action action)
        {
            var clickAction = new Button.ButtonClickedEvent();
            clickAction.AddListener(new UnityEngine.Events.UnityAction(action));
            PushButton.onClick = clickAction;
        }
    }
}