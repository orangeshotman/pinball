﻿using PinBall.Resources;
using System;
using UnityEngine;

namespace PinBall.UI
{
    public class UIMediator
    {
        private UIBehaviour _uiBehaviour;
      
        public UIMediator(PrefabProvider provider)
        {
            var canvas = UnityEngine.Object.Instantiate(provider.Data.Canvas);
            var go = UnityEngine.Object.Instantiate(provider.Data.UIRoot, canvas.transform, false) as GameObject;
            _uiBehaviour = go.GetComponent<UIBehaviour>();
        }

        public void SetExitButtonAction(Action exitAction)
        {
            _uiBehaviour.SetExitButtonAction(exitAction);
        }

        public void SetControllActions(Action leftButtonDown, Action rightButtonDown, Action leftButtonUp, Action rightButtonUp)
        {
            _uiBehaviour.SetButtonsControll(leftButtonDown,rightButtonDown,leftButtonUp,rightButtonUp);
        }

        public void SetStartMenuActions(Action startGameNoAI, Action startGameWithAI)
        {
            _uiBehaviour.SetStartMenuButtons(startGameNoAI, startGameWithAI);
        }

        public void SetActivePushPanel(bool val)
        {
            _uiBehaviour.SetActivePusherPanel(val);
        }

        public void SetPushButtonAction(Action action)
        {
            _uiBehaviour.SetPushButtonAction(action);
        }

        public void SetScrollAction(Action<float> action)
        {
            _uiBehaviour.SetScrollAction(action);
        }
    }
}