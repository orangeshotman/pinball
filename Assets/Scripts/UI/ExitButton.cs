﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PinBall.UI
{
    public class ExitButton : MonoBehaviour
    {
        public void Exit()
        {
            Application.Quit();
        }
    }
}
