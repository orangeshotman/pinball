﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace PinBall.SceneObjects
{
    public class Ball : MonoBehaviour
    {

        public event Action<Flipper> OnControllElementDetected = (x) => { };
        public event Action<Flipper> OnControllElementDispose = (x) => { };
        public event Action OnBallPusherreached = () => { };
        private Vector3 prevPosition;
        private Flipper _currentFlipper;
        private Rigidbody _rigidbody;
        void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            Application.targetFrameRate = 40;
            prevPosition = transform.position;
        }

        void Update()
        {
            RaycastHit hit;
            if (Physics.SphereCast(prevPosition, 1, transform.position - prevPosition, out hit, 1.2f))
            {
                var flipper = hit.collider.gameObject.GetComponent<Flipper>();
                if (flipper != null && _currentFlipper != flipper)
                {
                    if (_currentFlipper != null)
                        OnControllElementDispose(_currentFlipper);
                    _currentFlipper = flipper;
                    OnControllElementDetected(flipper);
                }
                else if (flipper == null && _currentFlipper != null)
                {
                    OnControllElementDispose(_currentFlipper);
                    _currentFlipper = null;
                }
                Debug.DrawLine(prevPosition, hit.point, Color.red);
            }
            else if (_rigidbody.velocity.magnitude <= 0.01 && _currentFlipper != null)
            {
                OnControllElementDispose(_currentFlipper);
                _currentFlipper = null;
            }
            prevPosition = transform.position;
        }
    }
}