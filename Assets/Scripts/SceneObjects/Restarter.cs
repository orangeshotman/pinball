﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PinBall.SceneObjects
{
    public class Restarter : MonoBehaviour
    {
        public event Action<Collider> OnTrigerEnter = (x) => { };

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == 8)
            {
                OnTrigerEnter(other);
            }
        }
    }
}