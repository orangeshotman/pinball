﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PinBall.SceneObjects
{
    public class Flipper : MonoBehaviour
    {
        public string InputName;
        [SerializeField]
        private float _targetVelocity;
        private HingeJoint _hingeJoint;

        void Awake()
        {
            _hingeJoint = GetComponent<HingeJoint>();
        }

        public void ToForward(string inputName)
        {
            if (inputName != InputName)
                return;
            var motor = _hingeJoint.motor;
            motor.targetVelocity = _targetVelocity;
            _hingeJoint.motor = motor;
        }

        public void ToBack(string inputName)
        {
            if (inputName != InputName)
                return;
            var motor = _hingeJoint.motor;
            motor.targetVelocity = -_targetVelocity;
            _hingeJoint.motor = motor;
        }

    }
}