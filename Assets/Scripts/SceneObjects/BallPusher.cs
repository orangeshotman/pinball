﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PinBall.SceneObjects
{
    public class BallPusher : MonoBehaviour
    {
        public event Action<BallPusher> OnBallStay = (x) => { };
        public event Action<bool> OnBallEnter = (b) => { };
        public string ButtonControll;
        public float PowerValue = 1;
        private string _pressedButton;

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == 8)
                OnBallEnter(true);
        }
        void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer == 8)
                OnBallEnter(false);
        }
        void OnTriggerStay(Collider other)
        {
            if (other.gameObject.layer == 8)
                OnBallStay(this);
            if (_pressedButton == ButtonControll)
            {
                if (other.gameObject.layer == 8)
                {
                    other.GetComponent<Rigidbody>().AddForce(Vector3.forward * PowerValue, ForceMode.Impulse);
                }
                _pressedButton = string.Empty;

            }
        }

        public void Push(string buttonName)
        {
            if (buttonName == ButtonControll)
            {
                _pressedButton = buttonName;
            }
        }

    }
}