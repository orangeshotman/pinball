﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PinBall.SceneObjects
{
    public class Table : MonoBehaviour
    {
        public Flipper[] Flippers;
        public Ball Ball;
        public BallPusher BallPusher;
        [SerializeField]
        private Restarter _restarter;

        void Awake()
        {
            _restarter.OnTrigerEnter += PutBallToStartPosition;
        }

        void OnDestroy()
        {
            _restarter.OnTrigerEnter -= PutBallToStartPosition;
        }

        private void PutBallToStartPosition(Collider collider)
        {
            var rig = collider.GetComponent<Rigidbody>();
            collider.transform.position = BallPusher.transform.position;
            rig.velocity = Vector3.zero;
        }
    }
}