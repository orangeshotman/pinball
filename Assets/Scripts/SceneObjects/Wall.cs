﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PinBall.SceneObjects
{
    public class Wall : MonoBehaviour
    {
        [SerializeField]
        private float AddForce;

        void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.gameObject.layer == 8)
            {
                var rig = collision.collider.GetComponent<Rigidbody>();
                rig.AddForce(rig.velocity.normalized * AddForce, ForceMode.Impulse);
            }
        }
    }
}