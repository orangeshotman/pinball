﻿namespace PinBall.Resources
{
    public class PrefabProvider 
    {
        private readonly PrefabProviderScriptable _data;
        private const string PrefabProviderScriptable = "PrefabProviderScriptable";

        public PrefabProviderScriptable Data
        {
            get { return _data; }
        }

        public PrefabProvider()
        {
            _data = UnityEngine.Resources.Load<PrefabProviderScriptable>(PrefabProviderScriptable);
        }
    }
}
