using UnityEngine;

namespace PinBall.Resources
{
    [CreateAssetMenu(fileName = "PrefabProviderScriptable.asset", menuName = "PrefabProviderScriptable")]
    public class PrefabProviderScriptable : ScriptableObject
    {
        public GameObject UIRoot;
        public GameObject Canvas;
        public GameObject Table;        
    }
}