﻿using UnityEngine;
using PinBall.Resources;
using PinBall.UI;
using PinBall.Input;
using PinBall.AI;
using PinBall.SceneObjects;
namespace PinBall.Root
{
    public class Application
    {
        private InputController _inputController;
        private PrefabProvider _prefabProvider;
        private Table _pinBallTable;
        private AIController _aiController;
        private UIMediator _uiMediator;
        public Application(InputController inputController, PrefabProvider prefabProvider, AIController aiController, UIMediator uiMediator)
        {
            _inputController = inputController;
            _prefabProvider = prefabProvider;
            _aiController = aiController;
            _uiMediator = uiMediator;
            _uiMediator.SetControllActions(
                () => { inputController.PressButton("Left"); },
                () => { inputController.PressButton("Right"); },
                () => { inputController.UpButton("Left"); },
                () => { inputController.UpButton("Right"); }
            );
            _uiMediator.SetPushButtonAction(() => { inputController.PressButton("Space"); });
            _uiMediator.SetStartMenuActions(StartGame, () => { StartGame(); _aiController.EnableAI(_pinBallTable); });
            _uiMediator.SetExitButtonAction(StopGame);

        }

        public void StartGame()
        {
            var go = Object.Instantiate(_prefabProvider.Data.Table);
            _pinBallTable = go.GetComponent<Table>();
            _inputController.ControllTable(_pinBallTable);
            
        }

        public void StopGame()
        {
            if (_pinBallTable != null)
            {
                _inputController.DisposeTable(_pinBallTable);
                _aiController.DisableAI();
                Object.Destroy(_pinBallTable.gameObject);
                
            }
        }
    }
}
