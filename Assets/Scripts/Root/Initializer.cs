﻿using Ninject;
using UnityEngine;
using PinBall.UI;
using PinBall.Input;
using PinBall.AI;

namespace PinBall.Root
{
    public class Initializer : MonoBehaviour
    {
        private IKernel _container;
        private Application _app;
        private void Awake()
        {
            ContainerSetup();
            Binds();
            ApplicationRun();

        }

        private void ContainerSetup()
        {
            NinjectSettings settings = new NinjectSettings();
            settings.LoadExtensions = false;
            settings.AllowNullInjection = false;
            settings.UseReflectionBasedInjection = false;
            _container = new StandardKernel(settings);
        }

        private void Binds()
        {
            _container.Bind<UIMediator>().ToSelf().InSingletonScope();
            _container.Bind<InputController>().ToSelf().InSingletonScope();
            _container.Bind<AIController>().ToSelf().InSingletonScope();
            _container.Bind<Application>().ToSelf().InSingletonScope();           
        }

        private void ApplicationRun()
        {
            _app = _container.Get<Application>();
        }
    }

    
}