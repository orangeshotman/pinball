﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PinBall.Resources;
using System;
using PinBall.UI;
using PinBall.SceneObjects;
namespace PinBall.Input
{
    public class InputController
    {
        public event Action<string> OnInputDown
        {
            add
            {
                _inputListener.OnInputDown += value;
            }
            remove
            {
                _inputListener.OnInputDown -= value;
            }
        }
        public event Action<string> OnInputUp
        {
            add
            {
                _inputListener.OnInputUp += value;
            }
            remove
            {
                _inputListener.OnInputUp -= value;
            }
        }
        private InputListener _inputListener;
        private UIMediator _uiMediator;
        public InputController(UIMediator uiMediator)
        {
            _uiMediator = uiMediator;
            var go = new GameObject();
            go.name = "InputListener";
            _inputListener = go.AddComponent<InputListener>();
        }

        public void PressButton(string buttonName)
        {
            _inputListener.PressButton(buttonName);
        }

        public void UpButton(string buttonName)
        {
            _inputListener.UpButton(buttonName);
        }

        public void ControllTable(Table table)
        {
            _uiMediator.SetScrollAction((x) => { table.BallPusher.PowerValue = x; });
            table.BallPusher.OnBallEnter += _uiMediator.SetActivePushPanel;
            OnInputDown += table.BallPusher.Push;
            foreach (var f in table.Flippers)
            {
                OnInputDown += f.ToForward;
                OnInputUp += f.ToBack;
            }
        }

        public void DisposeTable(Table table)
        {
            table.BallPusher.OnBallEnter -= _uiMediator.SetActivePushPanel;
            OnInputDown -= table.BallPusher.Push;
            foreach (var f in table.Flippers)
            {
                OnInputDown -= f.ToForward;
                OnInputUp -= f.ToBack;
            }
        }


    }
}