﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PinBall.Input
{
    public class InputListener : MonoBehaviour
    {

        public event Action<string> OnInputDown = (x) => { };
        public event Action<string> OnInputUp = (x) => { };
        private string[] _inputButtons = { "Left", "Right", "Space" };

        void Update()
        {
            foreach (var s in _inputButtons)
            {
                if (UnityEngine.Input.GetButtonDown(s))
                {
                    OnInputDown(s);
                }
                if (UnityEngine.Input.GetButtonUp(s))
                {
                    OnInputUp(s);
                }
            }
        }

        public void PressButton(string buttonName)
        {
            OnInputDown(buttonName);
        }

        public void UpButton(string buttonName)
        {
            OnInputUp(buttonName);
        }
    }
}